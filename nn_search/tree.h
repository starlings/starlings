#ifndef TREE_H
#define TREE_H

#include "../classes.h"
 #include "../geometry/vec.cpp"
 #include "node.cpp"
 
// TODO: pointers for branches and birds for leaves???

template <unsigned int dimensions, class my_bird> class tree
{
	node<my_bird> ** nodes;
	
	unsigned int levels;
	double * box_edge;
	vec<dimensions> ** centers;
	
	
public:
	tree(){};
	~tree();
	tree(const unsigned int &,const double &);
	
	bool _is_point_in_node(const vec<dimensions> &,const int &, const int &) const;
	unsigned int _is_box_in_radius(const vec<dimensions> &, const double &, const int &, const int &) const;
	void _recurr(vector<my_bird> &, const vec<dimensions> &, const double &, const int &, const int &) const;
	
	vector<my_bird> birds_in_radius(const vec<dimensions> & v, const double &) const;
	
	void _clear_nodes();
	void _add_bird(const my_bird &);
	
	void build_tree(const flock<dimensions,my_bird> &);
	void rebuild_tree(const flock<dimensions,my_bird> &);
	
	friend my_bird;
};


#endif
