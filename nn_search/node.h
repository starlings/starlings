#ifndef NODE_H
#define NODE_H

#include "../classes.h"

template <class my_bird> class node
{
	vector<my_bird> bird_list; /*Or pointers?*/
public:
	node();
	node(unsigned int size);
	
	void add_bird( const my_bird &);
	void clear();
	
	vector<my_bird> birds();
};

#endif 
