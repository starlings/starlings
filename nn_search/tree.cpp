#ifndef TREE_CPP
#define TREE_CPP

#include "tree.h"

template <unsigned int dimensions, class my_bird> tree<dimensions,my_bird>::~tree()
{
	delete [] box_edge;
	for(int i = 0; i < levels; i++)
	{
		delete [] centers[i];
		delete [] nodes[i];
	}
	delete [] centers;
	delete [] nodes;
}

template <unsigned int dimensions, class my_bird> tree<dimensions,my_bird>::tree(const unsigned int & my_levels, const double & my_edge)
{
	this->levels = my_levels;
	// Initialize edges
	this->box_edge = new double [this->levels];
	for(int i = 0; i < this->levels; i++)
	{
		this->box_edge[i] = my_edge/(0x1 << i);
	}
	// Initialize nodes
	this->nodes = new node<my_bird> * [this->levels];
	for(int i = 0; i < this->levels; i++)
	{
		this->nodes[i] = new node<my_bird> [( 0x1 << i*dimensions)];
	}
	// Initialize centers
	this->centers = new vec<dimensions> * [this->levels];
	for(int i = 0; i < this->levels; i++)
	{
		this->centers[i] = new vec<dimensions> [(0x1 << i*dimensions)];
		for(int j = 0; j < (0x1 << i*dimensions); j++)
		{
			int copy_j = j;
			for(int d = 0; d < dimensions; d++)
			{
				centers[i][j].set(d, ((copy_j % (0x1 << i)) +0.5)*this->box_edge[i] - this->box_edge[0]/2 );
				copy_j = floor(copy_j/ pow(2,i));
			}
		}
	}
}

// YET TO BE TESTED
template <unsigned int dimensions, class my_bird> bool tree<dimensions,my_bird>::_is_point_in_node(const vec<dimensions> & v, const int & i, const int & j) const
{
	vec<dimensions> dist = this->centers[i][j] - v;
	dist.keep_in_square(box_edge[0]);
	bool is_in = true;
	for(int d = 0; d < dimensions; d++)
	{
		if( (dist.component(d) > this->box_edge[i]/2) || (dist.component(d) <= - this->box_edge[i]/2) )
		{
			is_in = false;
			break;
		}
	}
	return is_in;
}

// YET TO BE TESTED:
//  0 = box outside sphere
//  1 = box inside sphere
//  2 = box partially inside -> recurr
template <unsigned int dimensions, class my_bird> unsigned int tree<dimensions,my_bird>::_is_box_in_radius(const vec<dimensions> & v, const double & r, const int & i, const int & j) const
{
	vec<dimensions> dist = this->centers[i][j] - v;
	dist.keep_in_square(this->box_edge[0]);
	vec<dimensions> radius = dist*r/!(dist);
	if(this->_is_point_in_node(radius + v, i, j))
		return 2;
	else
	{
		if( r*r >  ~(dist))
		{
			if(r > !(dist) + sqrt(dimensions)*this->box_edge[i]/2) // CHECK!
				return 1;
			else
				return 2;
		}
		else
			return 0;
	}
}

// TO BE TESTED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
template <unsigned int dimensions, class my_bird> void tree<dimensions,my_bird>::_recurr(vector<my_bird> & birds, const vec<dimensions> & v, const double & r, const int & i, const int & j) const
{
	
	unsigned int result = this->_is_box_in_radius(v,r,i,j);
	if(result)
	{
		if(result == 1)
		{
			for(int k = 0; k < nodes[i][j].birds().size(); k++)
				birds.push_back(nodes[i][j].birds()[k]);
		}
		else
		{
			if(i == this->levels - 1)
			{
				for(int k = 0; k < nodes[i][j].birds().size(); k++)
					birds.push_back(nodes[i][j].birds()[k]);
			}
			else
			{
				for(int k = 0; k < (0x1<< dimensions); k++)
				{
					int new_j = 0;
					for(int d = 0; d < dimensions; d++)
					{
						new_j += ( j & (((1 << i) - 1) << i*d)) << 1+d;
						if(k & (1 << d))
							new_j += (1 << (i+1)*d); 
					}
					this->_recurr(birds, v,r,i+1, new_j);
				}
			}
		}
	}
}

//TO BE TESTED
template <unsigned int dimensions, class my_bird> vector<my_bird> tree<dimensions,my_bird>::birds_in_radius(const vec<dimensions> & v, const double & r) const
{
	vector<my_bird> result;
	this->_recurr(result, v, r, 0, 0);
	return result;
}

template <unsigned int dimensions, class my_bird> void tree<dimensions,my_bird>::_clear_nodes()
{
	for(int i = 0; i < levels; i++)
		for(int j = 0; j < (1 << i*dimensions); j++)
			nodes[i][j].clear();
}

template <unsigned int dimensions, class my_bird> void tree<dimensions,my_bird>::_add_bird(const my_bird & b)
{
	vec <dimensions> position = b.get_position();
	for(int i = 0; i < this->levels; i++)
	{
		int j = 0;
		for(int d = 0; d < dimensions; d++)
		{
			j += floor((position.component(d) +this->box_edge[0]/2)/ (this->box_edge[i])) * (1 << d*i);
		}
		this->nodes[i][j].add_bird(b);
	}
}

template <unsigned int dimensions, class my_bird> void tree<dimensions,my_bird>::build_tree(const flock<dimensions,my_bird> & f)
{
	for(int i = 0; i < f.bird_number ; i++)
		this->_add_bird(f.bird_list[i]);
}

template <unsigned int dimensions, class my_bird> void tree<dimensions,my_bird>::rebuild_tree(const flock<dimensions,my_bird> & f)
{
	this->_clear_nodes();
	for(int i = 0; i < f.bird_number; i++)
		this->_add_bird(f.bird_list[i]);
}

#endif