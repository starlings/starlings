#ifndef NODE_CPP
#define NODE_CPP

#include "node.h"

template <class my_bird> node<my_bird>::node()
{
}

template <class my_bird> node<my_bird>::node(unsigned int size)
{
	this->bird_list.resize(size);
}

template <class my_bird> void node<my_bird>::add_bird(const my_bird & element)
{
	this->bird_list.push_back(element);
}

template <class my_bird> void node<my_bird>::clear()
{
	this->bird_list.clear();
}

template <class my_bird> vector<my_bird> node<my_bird>::birds()
{
	return this->bird_list;
}

#endif 
