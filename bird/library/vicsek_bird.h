#ifndef VICSEK_BIRD_H
#define VICSEK_BIRD_H

#include "../bird_base.cpp"

// Bird features
struct bird_features
{
	
};

// Flock features: to be delcared STATIC and to be re-declared outside the class.
struct flock_features
{
	static double interaction_radius;
	static double noise_level;
};
double flock_features::noise_level = 0.;
double flock_features::interaction_radius = 1.;

// Bird class
template <unsigned int dimensions> class vicsek_bird : public bird_base<dimensions>, public bird_features, public flock_features
{
public:
	vicsek_bird(){};
	
	// Evolution function
	vec<dimensions> evolve(const flock<dimensions, vicsek_bird> &);
	vec<dimensions> tree_evolve(const tree<dimensions,vicsek_bird> & );
};



#endif 
