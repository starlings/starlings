#ifndef SELF_PROPELLED_PARTICLES_H
#define SELF_PROPELLED_PARTICLES_H

#include "../bird_base.cpp"

template <unsigned int dimensions> class spp : public bird_base<dimensions>
{
public:
	
	static double noise_level;
	static double allignment_intensity;
	static double force_intensity;
	static double interaction_radius;
	static double allignment_radius; // less than interaction radius
	static double rest_radius; // less than interaction radius

	spp(){};
	
	vec<dimensions> tree_evolve(const tree<dimensions,spp> & );
};

template <unsigned int dimensions> double spp<dimensions>::noise_level = 0.;
template <unsigned int dimensions> double spp<dimensions>::allignment_intensity = 0.1;
template <unsigned int dimensions> double spp<dimensions>::force_intensity = 0.2;

template <unsigned int dimensions> double spp<dimensions>::interaction_radius = 5.;
template <unsigned int dimensions> double spp<dimensions>::allignment_radius= 2.;
template <unsigned int dimensions> double spp<dimensions>::rest_radius= 2.;

#endif
