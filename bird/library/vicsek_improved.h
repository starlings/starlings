#ifndef VICSEK_IMPROVED_H
#define VICSEK_IMPROVED_H

#include "../bird_base.cpp"

// Bird class
template <unsigned int dimensions> class vicsek_improved : public bird_base<dimensions>
{
public:
	
	static double interaction_radius;
	static double noise_level;

	vicsek_improved(){};
	
	// Evolution function
	vec<dimensions> tree_evolve(const tree<dimensions,vicsek_improved> & );
};

template <unsigned int dimensions> double vicsek_improved<dimensions>::noise_level = 0.;
template <unsigned int dimensions> double vicsek_improved<dimensions>::interaction_radius = 1.;

#endif // VICSEK_IMPROVED_H
