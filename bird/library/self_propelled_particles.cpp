#ifndef SELF_PROPELLED_PARTICLES_CPP
#define SELF_PROPELLED_PARTICLES_CPP

#include "self_propelled_particles.h"

template <unsigned int dimensions> vec<dimensions> spp<dimensions>::tree_evolve(const tree<dimensions, spp> & t)
{
	vec<dimensions> new_velocity = vec<dimensions>::null;
	vec<dimensions> my_position = this->position;
	double radius = spp<dimensions>::interaction_radius;

	vector<spp<dimensions>> bird_list = t.birds_in_radius(my_position, radius);

	vec<dimensions> * new_pbc_positions = new vec<dimensions> [bird_list.size()];
	int seed = 0;
	
	for(int i = 0; i < bird_list.size(); i++)
	{
		new_pbc_positions[i] = bird_list[i].position - this->position;
		new_pbc_positions[i].keep_in_square(t.box_edge[0]);
	}
	
	for(int i = 0; i < bird_list.size(); i++)
	{
		double square_bird_position = ~(new_pbc_positions[i]);
		double bird_position = sqrt(square_bird_position);
		if( (bird_position < spp<dimensions>::interaction_radius) && (bird_position != 0) )
		{
			// Linear Interaction
			//new_velocity += new_pbc_positions[i] * (spp<dimensions>::force_intensity * (  1 -  spp<dimensions>::rest_radius/bird_position)/(spp<dimensions>::interaction_radius - spp<dimensions>::rest_radius));
			
			// Inverse Power Interaction
			new_velocity += new_pbc_positions[i] * (((spp<dimensions>::force_intensity * spp<dimensions>::interaction_radius / bird_position) / (spp<dimensions>::interaction_radius - spp<dimensions>::rest_radius )) * (1 - spp<dimensions>::rest_radius / bird_position));
			
			if(bird_position < spp<dimensions>::allignment_radius)
			new_velocity+= bird_list[i].velocity * spp<dimensions>::allignment_intensity;
			
		}
		
		if(bird_position == 0)
		{
			seed = i;
			new_velocity += bird_list[i].velocity;
		}
	}
	
	#ifdef PREDATOR
		new_velocity += this->predator_escape_velocity();
	#endif
	
	delete [] new_pbc_positions; 
	
	if(~(new_velocity)==0)
		new_velocity = this->velocity;
	else
		new_velocity /= !(new_velocity);
	
	if(spp<dimensions>::noise_level == 0.)
		return new_velocity;
	else
	{
	vec<dimensions>::random::enable(seed);
	new_velocity += vec<dimensions>::random::polar_sphere(spp<dimensions>::noise_level );
	vec<dimensions>::random::disable();
	return (new_velocity/(!(new_velocity)));
	}
}

#endif 