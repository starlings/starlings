#ifndef SPP_V_H
#define SPP_V_H

#include "../bird_base.cpp"

template <unsigned int dimensions> class spp_nn_v : public bird_base<dimensions>
{
	double max_nn_distance = 0.;
public:
	
	static double noise_level;
	static double allignment_intensity;
	static double force_intensity;
	static double interaction_radius;
	static double allignment_radius; // less than interaction radius
	static double rest_radius; // less than interaction radius
	static unsigned int nn_number;

	spp_nn_v(){};
	spp_nn_v(const spp_nn_v &);
	void operator = (const spp_nn_v &);
	
	vec<dimensions> tree_evolve(const tree<dimensions,spp_nn_v> & );
};

template <unsigned int dimensions> double spp_nn_v<dimensions>::noise_level = 0.;
template <unsigned int dimensions> double spp_nn_v<dimensions>::allignment_intensity = 0.1;
template <unsigned int dimensions> double spp_nn_v<dimensions>::force_intensity = 0.3;

template <unsigned int dimensions> double spp_nn_v<dimensions>::interaction_radius = 3.;
template <unsigned int dimensions> double spp_nn_v<dimensions>::allignment_radius= 1.;
template <unsigned int dimensions> double spp_nn_v<dimensions>::rest_radius= 1.;

template <unsigned int dimensions> unsigned int spp_nn_v<dimensions>::nn_number= 7;

#endif // SPP_NEAREST_NEIGHBOUR_H
