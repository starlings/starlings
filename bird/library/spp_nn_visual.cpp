#ifndef SPP_V_CPP
#define SPP_V_CPP

#include "spp_nn_visual.h"

template <unsigned int dimensions> spp_nn_v<dimensions>::spp_nn_v(const spp_nn_v<dimensions> & to_copy)
{
	this->position = to_copy.position;
	this->velocity = to_copy.velocity;
	this->max_nn_distance = to_copy.max_nn_distance;
}

template <unsigned int dimensions> void spp_nn_v<dimensions>::operator = (const spp_nn_v<dimensions> & to_copy)
{
	this->position = to_copy.position;
	this->velocity = to_copy.velocity;
	this->max_nn_distance = to_copy.max_nn_distance;
}

template <unsigned int dimensions> vec<dimensions> spp_nn_v<dimensions>::tree_evolve(const tree<dimensions, spp_nn_v> & t)
{
	vec<dimensions> new_velocity = vec<dimensions>::null;
	double new_max_distance = 0;
	vector<spp_nn_v<dimensions>> bird_list = t.birds_in_radius(this->position, this->max_nn_distance);

	vec<dimensions> * new_pbc_positions = new vec<dimensions> [bird_list.size()];
	vector<int> winners;
	double * distance = new double [bird_list.size()];
	int seed = 0;
	
	for(int i = 0; i < bird_list.size(); i++)
	{
		new_pbc_positions[i] = bird_list[i].position - this->position;
		new_pbc_positions[i].keep_in_square(t.box_edge[0]);
		distance[i] = ~(new_pbc_positions[i]);
		
		
		if(distance[i] != 0)
		{
			if(winners.size() == 0)
				winners.push_back(i);
			else
			{
				if((winners.size() < spp_nn_v<dimensions>::nn_number) || (distance[i] < distance[winners[0]]))
					for(int j = 0; j < winners.size(); j++)
						if( (distance[i] > distance[winners[j]]))
						{
							winners.insert(winners.begin() + j, i);
							if(winners.size() > spp_nn_v<dimensions>::nn_number)
								winners.erase(winners.begin());
							break;
						}
					if(distance[i] < distance[winners.back()])
					{
						winners.push_back(i);
						if(winners.size() > spp_nn_v<dimensions>::nn_number)
								winners.erase(winners.begin());
					}
			}
		}
		else
		{
			seed = i;
		}
	}
	
	if(winners.size() > 0)
		this->max_nn_distance = distance[winners[0]] + !(this->velocity)*TIMESTEP; // Rememba!
	else
		this->max_nn_distance = spp_nn_v<dimensions>::interaction_radius;

	for(int i = 0; i < winners.size(); i++)
	{
		double bird_position = sqrt(distance[winners[i]]);
		if( (bird_position < spp_nn_v<dimensions>::interaction_radius) && (bird_position != 0) )
		{
			double angular_coefficient = (0.5 - (new_pbc_positions[winners[i]] * this->velocity)/(!(new_pbc_positions[winners[i]])* 2* (!(this->velocity))));
			new_velocity += new_pbc_positions[winners[i]] * angular_coefficient * (((spp_nn_v<dimensions>::force_intensity * spp_nn_v<dimensions>::interaction_radius / bird_position ) / (spp_nn_v<dimensions>::interaction_radius - spp_nn_v<dimensions>::rest_radius )) * (1 - spp_nn_v<dimensions>::rest_radius / bird_position));
			
			if(bird_position < spp_nn_v<dimensions>::allignment_radius)
			new_velocity+= bird_list[winners[i]].velocity * angular_coefficient *  spp_nn_v<dimensions>::allignment_intensity;
			
		}
	}
	
	#ifdef PREDATOR
		new_velocity += this->predator_escape_velocity();
	#endif
	
	delete [] new_pbc_positions; 
	delete [] distance;
	
	new_velocity += this->velocity;
	
	if(~(new_velocity)==0)
		new_velocity = this->velocity;
	else
		new_velocity /= !(new_velocity);
	
	if(spp_nn_v<dimensions>::noise_level == 0.)
		return new_velocity;
	else
	{
	vec<dimensions>::random::enable(seed);
	new_velocity += vec<dimensions>::random::polar_sphere(spp_nn_v<dimensions>::noise_level );
	vec<dimensions>::random::disable();
	return (new_velocity/(!(new_velocity)));
	}
}

#endif 