#ifndef VICSEK_IMPROVED_CPP
#define VICSEK_IMPROVED_CPP

#include "vicsek_improved.h"

template <unsigned int dimensions> vec<dimensions> vicsek_improved<dimensions>::tree_evolve(const tree<dimensions, vicsek_improved> & t)
{
	vec<dimensions> new_velocity = vec<dimensions>::null;
	vec<dimensions> my_position = this->position;
	double radius = vicsek_improved<dimensions>::interaction_radius;

	vector<vicsek_improved<dimensions>> bird_list = t.birds_in_radius(my_position, radius);

	vec<dimensions> * new_pbc_positions = new vec<dimensions> [bird_list.size()];
	int seed = 0;
	
	for(int i = 0; i < bird_list.size(); i++)
	{
		new_pbc_positions[i] = bird_list[i].position - this->position;
		new_pbc_positions[i].keep_in_square(t.box_edge[0]);
	}
	
	for(int i = 0; i < bird_list.size(); i++)
	{
		if(~(new_pbc_positions[i]) < vicsek_improved<dimensions>::interaction_radius*vicsek_improved<dimensions>::interaction_radius )
			new_velocity+= bird_list[i].velocity;
		
		if(~(new_pbc_positions[i]) == 0)
			seed = i;
	}
	
	#ifdef PREDATOR
		new_velocity += this->predator_escape_velocity();
	#endif
	
	delete [] new_pbc_positions; 
	
	if(~(new_velocity)==0)
		new_velocity = this->velocity;
	else
		new_velocity /= !(new_velocity);
	
	if(vicsek_improved<dimensions>::noise_level == 0.)
		return new_velocity;
	else
	{
	vec<dimensions>::random::enable(seed);
	new_velocity += vec<dimensions>::random::polar_sphere(vicsek_improved<dimensions>::noise_level );
	vec<dimensions>::random::disable();
	return (new_velocity/(!(new_velocity)));
	}
}

#endif 
