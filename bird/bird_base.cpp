#ifndef BIRD_BASE_CPP
#define BIRD_BASE_CPP

#include "bird_base.h"

// Copy constructor

template <unsigned int dimensions> bird_base <dimensions> :: bird_base(const bird_base & to_copy)
{
	this->position = to_copy.position;
	this->velocity = to_copy.velocity;
}

// Getters

template <unsigned int dimensions> vec<dimensions> bird_base<dimensions>::get_position() const
{
	return this->position;
}

template <unsigned int dimensions> vec<dimensions> bird_base<dimensions>::get_velocity() const
{
	return this->velocity;
}

// Updating velocity and position

template <unsigned int dimensions> void bird_base<dimensions>::keep_in_square(const double & edge )
{
	this->position.keep_in_square(edge);
}

template <unsigned int dimensions> void bird_base <dimensions> :: set_velocity(const vec<dimensions> & v)
{
	this->velocity = v;
}

template <unsigned int dimensions> void bird_base <dimensions> :: set_position(const vec<dimensions> & v)
{
	this->position = v;
}

template <unsigned int dimensions> void bird_base<dimensions>::update_position(const double & timestep = 1. )
{
	this->position = (this->position + this->velocity * timestep);
}

// Random generators

template <unsigned int dimensions> void bird_base<dimensions>::uniform_gaussian(const double & edge, const double & sigma)
{
	this->position = vec<dimensions>::random::uniform_box(edge);
	this->velocity = vec<dimensions>::random::gaussian(sigma);
}

template <unsigned int dimensions> void bird_base<dimensions>::uniform_spherical(const double & edge, const double & radius)
{
	this->position = vec<dimensions>::random::uniform_box(edge);
	this->velocity = vec<dimensions>::random::polar_sphere(radius);
}

template <unsigned int dimensions> void bird_base<dimensions>::gaussian_spherical(const double & edge, const double & sigma, const double & radius)
{
	this->position = vec<dimensions>::random::gaussian_box(edge,sigma);
	this->velocity = vec<dimensions>::random::polar_sphere(radius);
}

template <unsigned int dimensions> void bird_base<dimensions>::gaussian_gaussian(const double & edge, const double & sigma_p, const double & sigma_v)
{
	this->position = vec<dimensions>::random::gaussian_box(edge,sigma_p);
	this->velocity = vec<dimensions>::random::gaussian(sigma_v);
}


#ifdef PREDATOR
template <unsigned int dimensions> void bird_base<dimensions>::predator_evolve(const double & edge)
{
	bird_base<dimensions>::flock_predator.evolve_vel();
	bird_base<dimensions>::flock_predator.update_pos(edge);
}

template <unsigned int dimensions> void bird_base<dimensions>::predator_guide(const double & edge, const double & direction)
{
	bird_base<dimensions>::flock_predator.set_vel(direction);
	bird_base<dimensions>::flock_predator.update_pos(edge);
}

template <unsigned int dimensions> vec<dimensions> bird_base<dimensions>::predator_escape_velocity()
{
	vec<dimensions> predator_distance = bird_base<dimensions>::flock_predator.position - this->position;
	if(~(predator_distance) < (bird_base<dimensions>::flock_predator.predator_radius*bird_base<dimensions>::flock_predator.predator_radius))
		return - predator_distance * bird_base<dimensions>::flock_predator.predator_fear / ~(predator_distance);
	else
		return vec<dimensions>::null;
}
#endif

//Overload of assignment operator
template <unsigned int dimensions> void bird_base<dimensions> :: operator = (const bird_base<dimensions> & b)
{
	this->position = b.position;
	this->velocity = b.velocity;
}

// Overload of output operator
template <unsigned int dimensions> ostream & operator << (ostream & out, const bird_base<dimensions> & my_bird)
{
	out << "Position: " << my_bird.position << "\tVelocity: " << my_bird.velocity;
	return out;
}


#endif