#ifdef PREDATOR

#ifndef PREDATOR_CPP
#define PREDATOR_CPP

#include "predator.h"

template<unsigned int dimensions> void predator<dimensions>::update_pos(const double & edge)
{
	this->position += this->velocity * TIMESTEP;
	this->position.keep_in_square(edge);
	this->time += TIMESTEP;
}

template<unsigned int dimensions> void predator<dimensions>::set_vel(const double & direction)
{
	this->velocity.set(1, sin(direction*0.05));
	this->velocity.set(0, cos(direction*0.05));
}

template<unsigned int dimensions> void predator<dimensions>::evolve_vel()
{
	this->velocity.set(1, sin(time * 0.1));
	this->velocity.set(0, cos(time * 0.1));
}
#endif

#endif
