#ifndef BIRD_BASE_H
#define BIRD_BASE_H

#include "../classes.h"
#include "../geometry/vec.cpp"

#ifdef PREDATOR
#include "predator.cpp"
#endif


template <unsigned int dimensions> class bird_base
{
protected:
	vec<dimensions> position;
	vec<dimensions> velocity;
	
#ifdef PREDATOR
public:
	static predator<dimensions> flock_predator;
private:
#endif
	
public:
	bird_base(){};
	bird_base(const bird_base &);
	
	// Getters
	inline vec<dimensions> get_position() const;
	inline vec<dimensions> get_velocity() const;
	// Setters
	inline void keep_in_square(const double &);
	inline void set_velocity(const vec<dimensions> &);
	inline void set_position(const vec<dimensions> &);
	inline void update_position(const double &);
	
	// Randomizers
	void uniform_gaussian(const double &, const double &); //  1) Uniform position, gaussian velocity with velmax.
	void uniform_spherical(const double &, const double &); //  2) Uniform position, spherical velocity, if uniform.
	void gaussian_spherical(const double &, const double &, const double &); //  3) Gaussian position (already in flock), spherical velocity.
	void gaussian_gaussian(const double &, const double &, const double &); //  3) Gaussian position (already in flock), gaussian velocity.
	
	// Derived classes need to expose an evolve method: vec<dimensions> evolve(flock *)
#ifdef PREDATOR
	static void predator_evolve(const double & );
	static void predator_guide(const double &, const double &);
	vec<dimensions> predator_escape_velocity();
#endif
	// Assignment operator
	void operator = (const bird_base &);
	//Output operator
	template <unsigned int dim>  friend ostream & operator << (ostream &, const bird_base<dim> & );
};

#ifdef PREDATOR
template<unsigned int dimensions> predator<dimensions> bird_base<dimensions>::flock_predator;
#endif

#endif
