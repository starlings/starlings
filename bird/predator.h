#ifdef PREDATOR

#ifndef PREDATOR_H
#define PREDATOR_H 

#include "../geometry/vec.cpp"

template <unsigned int dimensions> struct predator
{
	vec<dimensions> position = vec<dimensions>::null;
	vec<dimensions> velocity = vec<dimensions>::null;
	double time = 0;
	double predator_fear = 1.;
	double predator_radius = 4.;
	
	void evolve_vel();
	void set_vel(const double &);
	void update_pos(const double &);
	
	predator(){};
};

#endif

#endif