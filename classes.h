#ifndef CLASSES_H
#define CLASSES_H

#include <iostream>
#include <fstream>	//write on file
#include <stdio.h>
#include <string.h>
#include <cmath>
#include <random>     // for random placement of birds
#include <thread>       // for parallelization
#include <time.h>        // seeding random engine
#include <ctime>			// time elapsed measurements
#include <GL/glut.h>	// graphic
#include <omp.h>	// parallelizzazione
#include <vector>	// used in tree for nn-search

using namespace std;

// Template vector class
template <unsigned int> class vec;

// Template bird_base class
template <unsigned int> class bird_base;

// Derived bird classes
template <unsigned int> class vicsek_bird;
template <unsigned int> class vicsek_improved;
template <unsigned int> class spp;
template <unsigned int> class spp_nn;
template <unsigned int> class spp_nn_v;


// Template class flock
template <unsigned int, class > class flock;

// Graphics handling
template <unsigned int> class graphic;
// drawing_tools
template <unsigned int, class> class flock_drawing_tool;

// NN-search
template <unsigned int, class> class tree;
template <class> class node;

#endif
