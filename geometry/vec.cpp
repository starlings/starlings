#ifndef VEC_CPP
#define VEC_CPP

#include "vec.h"

// Creator and copy constructor

template <unsigned int dimensions> vec <dimensions> :: vec()
{
};

template <unsigned int dimensions> template <typename... doubles> vec <dimensions> :: vec(doubles... my_new_components)
{
    double new_components[] = {my_new_components...};
    memcpy(this->components, new_components, dimensions * sizeof(double));
};

template <unsigned int dimensions> vec <dimensions> :: vec(const vec & v)
{
    memcpy(this->components, v.components, dimensions * sizeof(double));
};

// Functions

template <unsigned int dimensions> vec <dimensions> vec <dimensions> :: nullify()
{
    vec <dimensions> result;
    for(int i = 0; i < dimensions; i++)
    {
        result.components[i] = 0;
    }
    return result;
}

// Getters

template <unsigned int dimensions> const double & vec <dimensions> :: component(const int & N) const
{
	return this->components[N];
};

template <unsigned int dimensions> double & vec <dimensions> :: component(const int & N)
{
	return this->components[N];
};

// Setters

template <unsigned int dimensions> void vec <dimensions> :: set(const int & N, const double & value)
{
	this->components[N] = value;
};

template <unsigned int dimensions> void vec <dimensions> :: keep_in_square(const double & edge)
{
	for(int d = 0; d < dimensions; d++)
	{
		if(this->components[d] > edge/2)
		{
			this->components[d] -= edge;
			this->keep_in_square(edge);
		}
		else
		{
			if(components[d] < -edge/2)
			{
				this->components[d] += edge;
				this->keep_in_square(edge);
			}
		}
	}
}

// Operators

template <unsigned int dimensions> vec <dimensions> vec <dimensions> :: operator + (const vec & v) const
{
	vec <dimensions> result;
	for(int i = 0; i < dimensions; i++)
	{
		result.components[i] = this->components[i] + v.components[i];
	}
	return result;
};

template <unsigned int dimensions> void vec <dimensions> :: operator += (const vec & v)
{
	for(int i = 0; i < dimensions; i++)
	{
		this->components[i] += v.components[i];
	}
};

template <unsigned int dimensions> vec <dimensions> vec <dimensions> :: operator - (const vec & v) const
{
	vec <dimensions> result;
	for(int i = 0; i < dimensions; i++)
	{
		result.components[i] = this->components[i] - v.components[i];
	}
	return result;
};

template <unsigned int dimensions> void vec <dimensions> :: operator -= (const vec & v)
{
	for(int i = 0; i < dimensions; i++)
	{
		this->components[i] -= v.components[i];
	}
};

template <unsigned int dimensions> vec <dimensions> vec <dimensions> :: operator - () const
{
	vec <dimensions> result;
	for(int i = 0; i < dimensions; i++)
	{
		result.components[i] = - this->components[i];
	}
	return result;
};

template <unsigned int dimensions> vec <dimensions> vec <dimensions> :: operator * (const double & d) const
{
	vec <dimensions> result;
	for(int i = 0; i < dimensions; i++)
	{
		result.components[i] = this->components[i] * d;
	}
	return result;
};

template <unsigned int dimensions> void vec <dimensions> :: operator *= (const double & d)
{
	for(int i = 0; i < dimensions; i++)
	{
		this->components[i] *= d;
	}
};

template <unsigned int dimensions> vec <dimensions> vec <dimensions> :: operator / (const double & d) const
{
    vec <dimensions> result;
	for(int i = 0; i < dimensions; i++)
	{
		result.components[i] = this->components[i] / d;
	}
	return result;
}

template <unsigned int dimensions> void vec <dimensions> :: operator /= (const double & d)
{
	for(int i = 0; i < dimensions; i++)
	{
		this->components[i] /= d;
	}
}

template <unsigned int dimensions> double vec <dimensions> :: operator * (const vec <dimensions> & v) const
{
    double result = 0;
    for(int i = 0; i < dimensions; i++)
    {
        result += this->components[i] * v.components[i];
    }
    return result;
}

template <unsigned int dimensions> double vec <dimensions> :: operator  ~() const
{
    return (*this)*(*this);
}

template <unsigned int dimensions> double vec <dimensions> :: operator  !() const
{
    return sqrt(~(*this));
}

template <unsigned int dimensions> vec <dimensions> operator * (const double & d, const vec <dimensions> & v) 
{
    return v * d;
}

// Assignment operator

template <unsigned int dimensions> void vec<dimensions> :: operator = (const vec<dimensions> & v)
{
	for(int i = 0; i < dimensions; i++)
	{
		this->components[i] = v.components[i];
	}
}

// Static null member

template <unsigned int dimensions> vec <dimensions> vec <dimensions> :: null = nullify();

// Random vectors generation

template <unsigned int dimensions> void vec <dimensions> :: random :: enable()
{
    vec :: random :: randomizer = new default_random_engine(clock() + omp_get_thread_num());
}

template <unsigned int dimensions> void vec <dimensions> :: random :: enable(const int & seed)
{
    vec :: random :: randomizer = new default_random_engine(clock() + omp_get_thread_num() + seed);
}


template <unsigned int dimensions> void vec <dimensions> :: random :: disable()
{
    delete vec :: random :: randomizer;
}

template <unsigned int dimensions> vec <dimensions> vec <dimensions> :: random :: uniform_box(const double & edge)
{
    vec v;
    uniform_real_distribution <double> distribution(-edge/2 , edge/2);
   for(int i = 0; i < dimensions; i++)
   {
       v.components[i] = distribution(*randomizer);
   }
    return v;
}

template <unsigned int dimensions> vec <dimensions> vec <dimensions> :: random :: gaussian(const double & sigma)
{
    vec v;
   normal_distribution <double> distribution(0., sigma);
   
   for(int i = 0; i < dimensions; i++)
   {
		v.components[i] = distribution(*randomizer);
   }
    return v;
}

template <unsigned int dimensions> vec <dimensions> vec <dimensions> :: random :: gaussian_box(const double & edge, const double & sigma)
{
    vec v;
   normal_distribution <double> distribution(0., sigma);
   
   for(int i = 0; i < dimensions; i++)
   {
       do
            v.components[i] = distribution(*randomizer);
        while(v.components[i] < -edge / 2. || v.components[i] > edge / 2.);
   }
    return v;
}

template <unsigned int dimensions> vec <dimensions> vec <dimensions> :: random :: polar_sphere(const double & radius)
{
    vec v;
    normal_distribution <double> distribution(0., 1.);
    for(int i = 0; i < dimensions; i++)
   {
        v.components[i] = distribution(*randomizer);
   }
    return v*radius/(!(v));
}

// Print

template <unsigned int dimensions> ostream & operator << (ostream & out, const vec <dimensions> & v)
{
    out << "(" << v.components[0];
    for(int i = 1; i < dimensions; i++)
    {
		out << ", " << v.components[i];
	};
    out << ")";
    
    return out;
}

#endif
