#ifndef VEC_H
#define VEC_H

#include "../classes.h"

template <unsigned int dimensions> class vec
{
	double components[dimensions];
	
public:
	// Creator and copy constructor (Destructor needed? Verify memory leak)
	
	vec();
	template <typename... doubles> vec(doubles...);
	vec(const vec &);
    
    // Functions
    
    inline static vec nullify();
    
    // Getters

    inline const double & component(const int &) const;
    inline double & component(const int &);
	
	// Setters
	
	inline void set(const int &, const double &);
	inline void keep_in_square(const double &);
    // Operators overload
    
    inline vec operator + (const vec &) const;
   inline void operator += (const vec &);
    
    inline vec operator - (const vec &) const;
    inline void operator -= (const vec &);
    
    inline vec operator - () const;
    
    inline vec operator * (const double &) const;
    inline void operator *= (const double &);
    
    inline vec operator / (const double &) const;
    inline void operator /= (const double &);
    
    inline double operator * (const vec &) const;
    
    inline double operator ~ () const;
    inline double operator ! () const;
    
	// Assignment operator
	
	void operator = (const vec &);
	
    // Static null member
    
    static vec <dimensions> null;
    
    // Random generators

    class random
    {
        static  __thread default_random_engine *randomizer;     // Thread local:  thread_local
        
    public:
        
        static void enable();
		static void enable(const int & );
        static void disable();
        
        static vec uniform_box(const double &);
		static vec gaussian(const double &);
        static vec gaussian_box(const double &, const double &);
        static vec polar_sphere(const double &);
    };

    // Printing

    template <unsigned int dim> friend ostream & operator << (ostream & out, const vec <dim> & v);
};

// Other side multiplication

template <unsigned int dimensions> vec <dimensions> operator * (const double &, const vec <dimensions> &) ;

// Static declarations

template <unsigned int dimensions> __thread  default_random_engine * vec<dimensions>::random::randomizer; // Thread local:  thread_local

#endif
