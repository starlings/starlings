#ifndef FLOCK_DRAWING_TOOL_CPP
#define FLOCK_DRAWING_TOOL_CPP

#include "flock_drawing_tool.h"

template <unsigned int dimensions, class my_bird> void flock_drawing_tool<dimensions,my_bird>::initialize_flock(flock<dimensions, my_bird> & my_reference_flock)
{
	my_flock = my_reference_flock;
}

template <unsigned int dimensions, class my_bird> void flock_drawing_tool<dimensions,my_bird>::initialize_display(int & argc, char **argv, int w, int h, const char * name)
{
	graphic<dimensions>::initialize(argc, argv);
	graphic<dimensions>::create_window(w,h,name);
	graphic<dimensions>::create_eye(my_flock.edge);
}

template <unsigned int dimensions, class my_bird> void flock_drawing_tool<dimensions,my_bird>::draw()
{
	glutDisplayFunc(_render_scene);
	glutIdleFunc(_render_scene);
	glutSpecialFunc(graphic<dimensions>::_process_special_keys);
	glutTimerFunc(20, _recursive_evolution,1);
	glutMainLoop();
}

template <unsigned int dimensions, class my_bird> void flock_drawing_tool<dimensions,my_bird>::_render_scene()
{
	graphic<dimensions>::_initialize_loop();
	graphic<dimensions>::_perspective_routine(my_flock.edge);
	graphic<dimensions>::_draw_cube(my_flock.edge);
	flock_drawing_tool<dimensions,my_bird>::_draw_flock();
	flock_drawing_tool<dimensions,my_bird>::_draw_velocities();
	
	graphic<dimensions>::_finalize_loop();
}

template <unsigned int dimensions, class my_bird> void flock_drawing_tool<dimensions,my_bird>::_recursive_evolution(int value)
{

	my_flock.tree_evolve(); //Clever evolution
#ifdef GUIDED
	my_flock.set_predator_direction(graphic<dimensions>::predator_direction);
#endif
	//my_flock.evolve(); //Stupid evolution
	glutPostRedisplay();
	glutTimerFunc(10, _recursive_evolution,1);
}

/*template <unsigned int dimensions, class my_bird> void flock_drawing_tool<dimensions,my_bird>::_draw_flock()
{	
	
	glColor3ub( 255, 255, 255 );
	glEnableClientState( GL_VERTEX_ARRAY );
	glVertexPointer( graphic<dimensions>::graph_dimensions, GL_DOUBLE, sizeof(my_bird), &(my_flock.bird_list[0]) );
	glPointSize( 100./ graphic<dimensions>::get_radius() );
	glDrawArrays( GL_POINTS, 0, my_flock.bird_number);
	glDisableClientState( GL_VERTEX_ARRAY );
}*/

template <unsigned int dimensions, class my_bird> void flock_drawing_tool<dimensions,my_bird>::_draw_velocities()
{
	glLineWidth(1.0);
	
	glBegin(GL_LINES);
	for(int i = 0; i < my_flock.bird_number; i++)
	{	
		graphic<dimensions>::_set_color((double *)&my_flock.bird_list[i]);
		graphic<dimensions>::_draw_vertex((double *)&my_flock.bird_list[i], 0.3 );
	}
	glEnd();
}

//Specializzazione 1D

template <unsigned int dimensions, class my_bird> void flock_drawing_tool<dimensions,my_bird>::_draw_flock()
{	
	
	glPointSize( 100./ graphic<dimensions>::get_radius() );
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_POINTS);
	  for(int i=0; i < my_flock.bird_number; i++)
	  {	
		//graphic<dimensions>::_set_color((double *)&my_flock.bird_list[i]);
		#ifdef BOXES
		graphic<dimensions>::_draw_vertex((double *)&my_flock.bird_list[i]);
		#endif
		
	  }
	  #ifdef PREDATOR
	  glColor3f(100 ,0, 0);
	  graphic<dimensions>::_draw_vertex((double*) & my_bird::flock_predator.position);
	  #endif
	  
	glEnd();
	
}

#endif
