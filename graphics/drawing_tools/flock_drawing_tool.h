#ifndef FLOCK_DRAWING_TOOL_H
#define FLOCK_DRAWING_TOOL_H

#include "../../classes.h"
#include "../graphics.cpp"

template <unsigned int dimensions, class my_bird>  class flock_drawing_tool
{
	
	static flock<dimensions, my_bird> my_flock;
	
public:
	
	// Drawing functions
	static void initialize_flock(flock<dimensions, my_bird> &);
	static void initialize_display(int &, char **, int, int, const char *);
	static void draw();
	
	// Graphic utilities
	inline static void _render_scene();
	inline static void _recursive_evolution(int);
	inline static void _draw_flock();
	inline static void _draw_velocities();
};

template <unsigned int dimensions, class my_bird> flock<dimensions, my_bird> flock_drawing_tool<dimensions, my_bird>::my_flock;




#endif 
