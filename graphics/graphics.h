#ifndef GRAPHIC_H
#define GRAPHIC_H

#include "../classes.h"

// See url:						http://www.lighthouse3d.com/tutorials/glut-tutorial/?2

template <unsigned int dimensions> class graphic
{
	static double x, y, z;
	static double theta, phi;
	static double radius;
	#ifdef GUIDED
	public:
	static double predator_direction;
	private:
	#endif

	
public:
	static int graph_dimensions;
	// Initializers
	static void initialize(int &, char **);	// Process main() arguments
	static void create_window(const unsigned int &, const unsigned int &, const char *);	// Creates window and initialize graphic
	static void create_eye(const double &);	// Initialize Perspective
	
	// Getters
	inline static double get_radius();

	// Utilities
	inline static void _initialize_loop(); // Clears scene redrawing
	inline static void _process_special_keys(int, int, int); // Used for change of perspective
	inline static void _draw_cube(double); // draws the cube of radius/2 edge
	inline static void _finalize_loop(); // Finish scene redrawing
	inline static void _draw_vertex(double *, double);
	inline static void _draw_vertex(double *);
	inline static void _set_color(double *);

	// Drawers void funcs
	inline static void _perspective_routine(double);
	
};

template <unsigned int dimensions> double graphic<dimensions>::x = 1.;
template <unsigned int dimensions> double graphic<dimensions>::y = 0.;
template <unsigned int dimensions> double graphic<dimensions>::z = 0.;
template <unsigned int dimensions> double graphic<dimensions>::theta = M_PI/2;
template <unsigned int dimensions> double graphic<dimensions>::phi = 0.;
template <unsigned int dimensions> double graphic<dimensions>::radius = 1.;
template <unsigned int dimensions> int graphic<dimensions>::graph_dimensions = 3;

#ifdef GUIDED
template <unsigned int dimensions> double graphic<dimensions>::predator_direction = 0;
#endif


//Specializzazione 2D

template <> double graphic<2>::x = 0.;
template <> double graphic<2>::y = 0.;
template <> int graphic<2>::graph_dimensions = 2;

//Specializzazione 1D
template <> int graphic<1>::graph_dimensions = 2;
#endif





