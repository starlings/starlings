#ifndef GRAPHIC_CPP
#define GRAPHIC_CPP

#include "graphics.h"

// General utilities #########################################################

template <unsigned int dimensions> void graphic<dimensions>::initialize(int & argc, char ** argv)
{
	glutInit(&argc, argv);
}

template <unsigned int dimensions> void graphic<dimensions>::create_window(const unsigned int & w,const unsigned int  & h,const char * name)
{
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(w,h);
	glutCreateWindow(name);
}

template <unsigned int dimensions> void graphic<dimensions>::create_eye(const double & my_radius)
{
	radius = my_radius;
	x = radius;
}

// Getters

template <unsigned int dimensions> double graphic<dimensions>::get_radius()
{
	return radius;
}

// Service Function ###########################################################

template <unsigned int dimensions> void graphic<dimensions>::_initialize_loop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

template <unsigned int dimensions> void graphic<dimensions>::_finalize_loop()
{
	glFlush();
	glutSwapBuffers();
}

template <unsigned int dimensions> void graphic<dimensions>::_process_special_keys(int key, int xx, int yy)
{
	float fraction = 0.05;
	switch (key) {
		case GLUT_KEY_LEFT :
			phi -= fraction;
			x = cos(phi)*sin(theta)*radius;
			y = sin(phi)*sin(theta)*radius;
			break;
			
		case GLUT_KEY_RIGHT :
			phi += fraction;
			x = cos(phi)*sin(theta)*radius;
			y = sin(phi)*sin(theta)*radius;
			break;
			
		case GLUT_KEY_DOWN:
			if(theta < M_PI - fraction)
			theta += fraction;
			z = cos(theta)*radius;
			x = cos(phi)*sin(theta)*radius;
			y = sin(phi)*sin(theta)*radius;
			break;
		case GLUT_KEY_UP:
			if(theta > fraction)
			theta -= fraction;
			z = cos(theta)*radius;
			x = cos(phi)*sin(theta)*radius;
			y = sin(phi)*sin(theta)*radius;
			break;

		case GLUT_KEY_F1 :
			radius += fraction*10;
			z = cos(theta)*radius;
			x = cos(phi)*sin(theta)*radius;
			y = sin(phi)*sin(theta)*radius;
			break;

		case GLUT_KEY_F2 :
			if(radius>fraction*10)
			radius -= fraction*10;
			z = cos(theta)*radius;
			x = cos(phi)*sin(theta)*radius;
			y = sin(phi)*sin(theta)*radius;
			break;
			
			
	}
}

template <unsigned int dimensions> void graphic<dimensions>::_draw_cube(double edge)
{
      glLineWidth(3);
      glBegin(GL_LINES);
      
	//Asse Z
	glColor3f(0.0, 255., 0.0);
	  glVertex3f(-edge/2, edge/2, -edge/2);
	  glVertex3f(-edge/2, -edge/2, -edge/2);
	  
	//Asse X
	glColor3f(255., 0.0, 0.0);
	  glVertex3f(edge/2, -edge/2, -edge/2);
	  glVertex3f(-edge/2, -edge/2, -edge/2);
	 
	//Asse Y  
	glLineWidth(5);
	glColor3f(0.0, 0.0, 255);
	  glVertex3f(-edge/2, -edge/2, edge/2);
	  glVertex3f(-edge/2, -edge/2, -edge/2);
	
      glEnd();
      
	//Altri
      glLineWidth(1.0);
      glColor3f(50, 50, 0.0);
      glBegin(GL_LINES);
	  
	glVertex3f(edge/2, edge/2, edge/2);
	glVertex3f(-edge/2, edge/2, edge/2);
	
	glVertex3f(edge/2, edge/2, edge/2);
	glVertex3f(edge/2, -edge/2, edge/2);
	
	glVertex3f(-edge/2, edge/2, edge/2);
	glVertex3f(-edge/2, -edge/2, edge/2);
	
	glVertex3f(edge/2, -edge/2, edge/2);
	glVertex3f(-edge/2, -edge/2, edge/2);
	
	glVertex3f(edge/2, edge/2, -edge/2);
	glVertex3f(-edge/2, edge/2, -edge/2);
	
	glVertex3f(edge/2, edge/2, -edge/2);
	glVertex3f(edge/2, -edge/2, -edge/2);
	
	glVertex3f(edge/2, edge/2, -edge/2);
	glVertex3f(edge/2, edge/2, edge/2);
	
	glVertex3f(-edge/2, edge/2, -edge/2);
	glVertex3f(-edge/2, edge/2, edge/2);
	
	glVertex3f(edge/2, -edge/2, edge/2);
	glVertex3f(edge/2, -edge/2, -edge/2);
	
	
      glEnd();
	
}

template <unsigned int dimensions> void graphic<dimensions>::_perspective_routine(double edge)
{
	
	glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-radius, radius, - radius, radius, -0.5*edge, 2*edge);
    
    gluLookAt(	x, y,  z,
							0., 0.,  0.,
							0., 0.,  1.);
							
}

template <unsigned int dimensions> void graphic<dimensions>::_draw_vertex(double * N_bird, double ratio)
{
	glVertex3f(*(N_bird), *(N_bird+1), *(N_bird+2));
	glVertex3f(*(N_bird)+(*(N_bird+dimensions))*ratio, *(N_bird+1)+(*(N_bird+dimensions+1))*ratio, *(N_bird+2)+(*(N_bird+dimensions+2))*ratio);
}

template <unsigned int dimensions> void graphic<dimensions>::_draw_vertex(double * N_bird)
{
	glVertex3f(*(N_bird), *(N_bird+1), *(N_bird+2));
}

template <unsigned int dimensions> void graphic<dimensions>::_set_color(double * N_bird)
{
	glColor3f(*(N_bird+dimensions+1), *(N_bird+dimensions+2), *(N_bird+dimensions+3));
}

//Specializzazione 3D

template <> void graphic<3>::_set_color(double * N_bird)
{
	glColor3f(255,0,0.8);
}

//Specializzazione 2D

template <> void graphic<2>::create_eye(const double & my_radius)
{
	radius = my_radius;
}

template <> void graphic<2>::_draw_cube(double edge)
{
{
	glLineWidth(1.0);
    glColor3f(50, 50, 0);
      glBegin(GL_LINES);
	glVertex2f(edge/2, edge/2);
	glVertex2f(-edge/2, edge/2);
	
	glVertex2f(edge/2, edge/2);
	glVertex2f(edge/2, -edge/2);
      glEnd();
	
	glLineWidth(3.0);
    glBegin(GL_LINES);
	glColor3f(1., 0.0, 0.0);
	glVertex2f(-edge/2, edge/2);
	glVertex2f(-edge/2, -edge/2);
	
	glColor3f(0.0, 1.0, 0.0);
	glVertex2f(-edge/2, -edge/2);
	glVertex2f(edge/2, -edge/2);
	
	glEnd();
}
}


template <> void graphic<2>::_perspective_routine(double edge)
{
	
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-radius, radius, - radius, radius, 0., 2*edge);

    gluLookAt(	x, y,  radius,
							x, y,  0.,
							0., 1.,  0.);
							
}

template <> void graphic<2>::_process_special_keys(int key, int xx, int yy)
{
	float fraction = 0.05;
	switch (key) {
		case GLUT_KEY_LEFT :
			x -= fraction;
			break;
			
		case GLUT_KEY_RIGHT :
			x += fraction;
			break;
			
		case GLUT_KEY_DOWN:
			y -= fraction;
			break;
			
		case GLUT_KEY_UP:
			y += fraction;
			break;

		case GLUT_KEY_F1 :
			radius += fraction*10;
			break;

		case GLUT_KEY_F2 :
			if(radius>fraction*10)
			radius -= fraction*10;
			break;
			
#ifdef GUIDED

		case GLUT_KEY_F3 :
			predator_direction++;
			break;

		case GLUT_KEY_F4 :
			predator_direction--;
			break;
#endif
			
	}
}

template <> void graphic<2>::_draw_vertex(double * N_bird, double ratio)
{
	glVertex2f(*(N_bird), *(N_bird+1));
	glVertex2f(*(N_bird)+(*(N_bird+2))*ratio, *(N_bird+1)+(*(N_bird+3))*ratio);
}

template <> void graphic<2>::_draw_vertex(double * N_bird)
{
	glVertex2f(*(N_bird), *(N_bird+1));
}

template <> void graphic<2>::_set_color(double * N_bird)
{
	glColor3f(255,0,0.8);
}

//Specializzazione 1D

template <> void graphic<1>::create_eye(const double & my_radius)
{
	radius = my_radius;
}

template <> void graphic<1>::_draw_cube(double edge)
{
{
	glLineWidth(1.0);
    glColor3f(0.0, 0.0, 0.8);
      
    glBegin(GL_LINES);
	glVertex2f(edge/2, 0);
	glVertex2f(-edge/2, 0);
      glEnd();
}
}


template <> void graphic<1>::_perspective_routine(double edge)
{
	
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-radius, radius, - radius, radius, 0., 2*edge);

    gluLookAt(	x, 0,  radius,
							x, 0,  0.,
							0., 1.,  0.);
							
}

template <> void graphic<1>::_process_special_keys(int key, int xx, int yy)
{
	float fraction = 0.05;
	switch (key) {
		case GLUT_KEY_LEFT :
			x -= fraction;
			break;
			
		case GLUT_KEY_RIGHT :
			x += fraction;
			break;
			

		case GLUT_KEY_F1 :
			radius += fraction*10;
			break;

		case GLUT_KEY_F2 :
			if(radius>fraction*10)
			radius -= fraction*10;
			break;
			
	}
}

template <> void graphic<1>::_draw_vertex(double * N_bird, double ratio)
{
	glVertex2f(*(N_bird), 0);
	glVertex2f(*(N_bird)+(*(N_bird+1))*ratio, 0);
}

template <> void graphic<1>::_draw_vertex(double * N_bird)
{
	glVertex2f(*(N_bird), 0.);
}

template <> void graphic<1>::_set_color(double * N_bird)
{
	glColor3f(255,0,0.8);
}


#endif
