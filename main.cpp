
// Model variables setting
#define DIM 2
#define BIRDS  1000
#define EDGE 50
#define TIMESTEP 0.1
#define NOISE 0.
#define LEVELS 7
//#define BOXES
//#define PREDATOR
//#define GUIDED

// Inclusions
#include "flock/flock.cpp"
#include "nn_search/tree.cpp"
#include "graphics/drawing_tools/flock_drawing_tool.cpp"

// Bird Library
#include "bird/library/vicsek_bird.cpp"
#include "bird/library/vicsek_improved.cpp"
#include "bird/library/self_propelled_particles.cpp"
#include "bird/library/spp_nearest_neighbour.cpp"
#include "bird/library/spp_nn_visual.cpp"

// File Output
#define NO_OUTPUT // Either BINARY_WRITE or TXT_WRITE, or NO_OUTPUT
#define VAL(str) #str
#define TOSTRING(str) VAL(str)
#define BIRD_MODEL spp_nn
#define STEPS 100


int main(int argc, char **argv)
{

	// Creating and initializiang flock
	flock <DIM,BIRD_MODEL<DIM>> my_flock(BIRDS, EDGE, LEVELS);
	my_flock.uniform_spherical(1.);
	
	// Setting parameters
	flock<DIM,BIRD_MODEL<DIM>>::set_timestep(TIMESTEP);
	BIRD_MODEL<DIM>::noise_level = NOISE;

#ifdef NO_OUTPUT	// Graphically represents the flock

	flock_drawing_tool<DIM, BIRD_MODEL<DIM>>::initialize_flock(my_flock);
	flock_drawing_tool<DIM, BIRD_MODEL<DIM>>::initialize_display(argc, argv, 960, 960, "Flock");
	flock_drawing_tool<DIM, BIRD_MODEL<DIM>>::draw();
	
#elif defined TXT_WRITE	// Writes function values on txt file

	char filename[100];
	int simulation;
	
	simulation = sprintf(filename, "Features_%s=%s_%s=%d %s=%d_%s=%d_%s=%g",VAL(BIRD_MODEL), TOSTRING (BIRD_MODEL), VAL(DIM), DIM, VAL(BIRDS), BIRDS, VAL(EDGE), EDGE, VAL(TIMESTEP), TIMESTEP);
	my_flock.txt_write_evolve(STEPS, filename);

#elif defined BINARY_WRITE	// Writes whole evolution on binary file
	
	char filename[100];
	int simulation;
	
	simulation = sprintf(filename, "Simulation_%s=%s_%s=%d %s=%d_%s=%d_%s=%d_%s=%g",VAL(BIRD_MODEL), TOSTRING (BIRD_MODEL), VAL(DIM), DIM, VAL(BIRDS), BIRDS, VAL(EDGE), EDGE, VAL(STEPS), STEPS, VAL(TIMESTEP), TIMESTEP);
	my_flock.binary_write_evolve(STEPS, filename);

#endif

}




