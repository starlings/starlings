#ifndef FLOCK_H
#define FLOCK_H

#include "../classes.h"
#include "../bird/bird_base.h"
#include "../nn_search/tree.cpp"

//void display(); ??? Is this needed somehow??? Don't think so...

// General class withou private or public features
template <unsigned int dimensions, class my_bird = bird_base<dimensions>> class flock
{
	unsigned int bird_number;
	my_bird * bird_list;
	vec<dimensions> *new_velocity;
	double edge;
	unsigned int levels;
	// Evolution parameters
	static double timestep;
	#ifdef GUIDED
	static double predator_direction;
	#endif
	
public:
	flock(){};
	~flock();
	flock(const flock &);
	flock(const unsigned int &, const double &, const unsigned int &);
	
	//Randomization of initial positions
	void uniform_gaussian(const double &);
	void uniform_spherical(const double &);
	void gaussian_spherical(const double &, const double &);
	void gaussian_gaussian(const double &, const double &);
	// Assignment operator
	void operator = (const flock &);
	// Output operator
	template <unsigned int dim, class my>  friend ostream & operator << (ostream &, const flock <dim, my> & );
	
	// Features estraction
	vec<dimensions> average_velocity();
	double velocity_std();
	
	// Evolution function:
	friend my_bird;
	
	void evolve();
	void tree_evolve();
	void txt_write_evolve(int, const char* ); //evolve and write features on file. (# of iteration, output file name)
	void binary_write_evolve(int, const char*); //evolve and write position&velocity on file (# of iteration, output file name)

#ifdef GUIDED
	static void set_predator_direction(const double & direction);
#endif

	static void set_timestep(const double &);
	
	// Graphics
friend class flock_drawing_tool<dimensions, my_bird>;

	// nn-search
	friend class tree<dimensions,my_bird>;
};

template <unsigned int dimensions, class my_bird> double flock<dimensions,my_bird>::timestep = 1.;
#ifdef GUIDED
template <unsigned int dimensions, class my_bird> double flock<dimensions,my_bird>::predator_direction = 0.;
#endif

#endif
