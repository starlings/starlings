#ifndef FLOCK_CPP
#define FLOCK_CPP

#include "flock.h"

// Destructor operator

template <unsigned int dimensions, class my_bird> flock<dimensions, my_bird>::~flock()
{
	delete [] bird_list;
	delete [] new_velocity;
}

// Copy constructor

template <unsigned int dimensions, class my_bird> flock<dimensions, my_bird>::flock(const flock & f)
{
	this->bird_number=f.bird_number;
	this->bird_list = new my_bird [bird_number];
	this->new_velocity = new vec<dimensions> [bird_number];
	this->edge = f.edge;
	this->levels = f.levels;
	memcpy(this->bird_list, f.bird_list, f.bird_number * sizeof(my_bird));
}

// Parametric constructor
template <unsigned int dimensions, class my_bird> flock<dimensions, my_bird>::flock(const unsigned int & num, const double & new_edge, const unsigned int & new_levels)
{
	this->bird_number = num;
	this->bird_list = new my_bird [num];
	this->new_velocity = new vec<dimensions> [num];
	this->levels = new_levels;
	this->edge = new_edge;
}

// Randomisation of positions
template <unsigned int dimensions, class my_bird> void flock<dimensions, my_bird>::uniform_gaussian( const double & sigma)
{
	vec<dimensions>::random::enable();
	for(int i = 0; i < this->bird_number; i++)
		this->bird_list[i].uniform_gaussian(this->edge,sigma);
	vec<dimensions>::random::disable();
}

template <unsigned int dimensions, class my_bird> void flock<dimensions, my_bird>::uniform_spherical( const double & radius)
{
	vec<dimensions>::random::enable();
	for(int i = 0; i < this->bird_number; i++)
		this->bird_list[i].uniform_spherical(this->edge,radius);
	vec<dimensions>::random::disable();
}

template <unsigned int dimensions, class my_bird> void flock<dimensions, my_bird>::gaussian_spherical( const double & sigma, const double & radius)
{
	vec<dimensions>::random::enable();
	for(int i = 0; i < this->bird_number; i++)
		this->bird_list[i].gaussian_spherical(this->edge,sigma,radius);
	vec<dimensions>::random::disable();
}

template <unsigned int dimensions, class my_bird> void flock<dimensions, my_bird>::gaussian_gaussian( const double & sigma_p, const double & sigma_v)
{
	vec<dimensions>::random::enable();
	for(int i = 0; i < this->bird_number; i++)
		this->bird_list[i].gaussian_gaussian(this->edge,sigma_p, sigma_v);
	vec<dimensions>::random::disable();
}
// Overload assignment operator

template <unsigned int dimensions, class my_bird> void flock<dimensions,my_bird>:: operator = (const flock & f)
{
	this->bird_number = f.bird_number;
	this->edge = f.edge;
	this->levels = f.levels;
	this->bird_list = new my_bird [f.bird_number];
	this->new_velocity = new vec<dimensions> [f.bird_number];
	for(int i = 0; i < f.bird_number; i++)
	{
		this->bird_list[i] = f.bird_list[i];
		this->new_velocity[i] = f.new_velocity[i];
	}
}

// Overload output operator

template <unsigned int dim, class my> ostream & operator << (ostream & out, const flock <dim, my> & f )
{
	for(int i = 0; i < f.bird_number; i++)
	{
		cout << "Bird[" << i << "]:\t" << f.bird_list[i] << endl;
	}
}

// Features extraction
 
template <unsigned int dimensions, class my_bird> vec<dimensions> flock<dimensions,my_bird>::average_velocity()
{
	vec<dimensions> avg = vec<dimensions>::null;
	for(int i = 0; i < this->bird_number; i++)
	{
		avg += (this->bird_list[i].get_velocity());
	}
	return avg/(this->bird_number);
}

template <unsigned int dimensions, class my_bird> double flock<dimensions,my_bird>::velocity_std()
{
	vec<dimensions> avg = this->average_velocity();
	double var = 0.;
	for(int i = 0; i < this->bird_number; i++)
	{
		var += ~(avg - this->bird_list[i].get_velocity());
	}
	return sqrt(var/(this->bird_number));
}
// Evolution function

template <unsigned int dimensions, class my_bird> void flock<dimensions,my_bird>:: evolve()
{	
	
	omp_set_num_threads(omp_get_num_procs());
	

	#pragma omp parallel for schedule(static)
	for(int i = 0; i < this->bird_number; i++)
	{	
		new_velocity[i] = bird_list[i].evolve(*this);
	}
	#pragma omp barrier

	for(int i = 0; i < this->bird_number; i++)
	{
		bird_list[i].set_velocity(new_velocity[i]);
		bird_list[i].update_position(flock<dimensions,my_bird>::timestep);
		bird_list[i].keep_in_square(this->edge);
	}
}

template <unsigned int dimensions, class my_bird> void flock<dimensions,my_bird>::tree_evolve()
{	
	
	omp_set_num_threads(omp_get_num_procs());
	
	tree<dimensions,my_bird> flock_tree(this->levels, this->edge);
	flock_tree.build_tree(*this);
	
	#pragma omp parallel for schedule(static) 
	for(int i = 0; i < this->bird_number; i++)
	{
		new_velocity[i] = bird_list[i].tree_evolve(flock_tree);
	}
	#pragma omp barrier

	for(int i = 0; i < this->bird_number; i++)
	{
		bird_list[i].set_velocity(new_velocity[i]);
		bird_list[i].update_position(flock<dimensions,my_bird>::timestep);
		bird_list[i].keep_in_square(this->edge);
	}
	
#ifdef GUIDED
	my_bird::predator_guide(this->edge, flock<dimensions,my_bird>::predator_direction); // Predator Guided
#elif defined PREDATOR
	my_bird::predator_evolve(this->edge); //Predator Aut-Evolution
#endif
	
}


template <unsigned int dimensions, class my_bird> void flock<dimensions,my_bird>:: txt_write_evolve(int steps,const char* name_file)
{	
	omp_set_num_threads(omp_get_num_procs());
	ofstream f(name_file, ios::binary); 
	
	for(int j = 0; j < steps; j++)
	{
	  
	  f << j << '\t' << velocity_std()<< '\t'<<  average_velocity() << '\n';
		
		tree<dimensions,my_bird> flock_tree(this->levels, this->edge);
		flock_tree.build_tree(*this);
		
		#pragma omp parallel for schedule(static)
		for(int i = 0; i < this->bird_number; i++)
		{	
			new_velocity[i] = bird_list[i].tree_evolve(flock_tree);
		}
		#pragma omp barrier

		for(int i = 0; i < this->bird_number; i++)
		{
			bird_list[i].set_velocity(new_velocity[i]);
			bird_list[i].update_position(flock<dimensions,my_bird>::timestep);
			bird_list[i].keep_in_square(this->edge);
		}
	}
	
}

	


template <unsigned int dimensions, class my_bird> void flock<dimensions,my_bird>:: binary_write_evolve(int steps,const char* name_file)
{	
	omp_set_num_threads(omp_get_num_procs());
	
	FILE* pFile;
	pFile = fopen(name_file, "wb");
	
	double * buffer = new double[dimensions*2*this->bird_number*steps];
	
	for(int j = 0; j < steps; j++)
	{
	 
		tree<dimensions,my_bird> flock_tree(this->levels, this->edge);
		flock_tree.build_tree(*this);
		
		#pragma omp parallel for schedule(static) shared(buffer)
		for(int i = 0; i < this->bird_number; i++)
			
		{
			new_velocity[i] = bird_list[i].tree_evolve(flock_tree);
			
			 #pragma omp critical
			  {
				for(int component = 0; component < dimensions; component++)
				{
				  buffer[(j*this->bird_number*2*dimensions)+(i*2*dimensions)+component] = bird_list[i].get_position().component(component);
				  buffer[(j*this->bird_number*2*dimensions)+(i*2*dimensions)+component+dimensions] = bird_list[i].get_velocity().component(component);
				}
			  }
		}
		#pragma omp barrier

		for(int i = 0; i < this->bird_number; i++)
		{
			bird_list[i].set_velocity(new_velocity[i]);
			bird_list[i].update_position(flock<dimensions,my_bird>::timestep);
			bird_list[i].keep_in_square(this->edge);
		}
	}
	
	
	//for(int b=0; b < (dimensions*2*this->bird_number*steps); b++) cout << buffer[b]<<endl;
	fwrite(buffer, sizeof(double), sizeof(buffer), pFile);
	fclose(pFile);
}

template <unsigned int dimensions, class my_bird> void flock<dimensions,my_bird>::set_timestep(const double & new_t)
{
	flock<dimensions,my_bird>::timestep = new_t;
}

#ifdef GUIDED
template <unsigned int dimensions, class my_bird> void flock<dimensions, my_bird>::set_predator_direction(const double & direction)
{
	flock<dimensions,my_bird>::predator_direction = direction;
}

#endif

#endif
